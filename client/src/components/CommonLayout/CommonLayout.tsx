import * as React from 'react';
import { Outlet } from 'react-router-dom';
import { ContentWrapper } from './CommonLayout.style';
import { Footer } from '../Footer/Index';
import { Header } from '../Header';

export const CommonLayout: React.FC = function CommonLayout() {
  return (
    <>
      <Header />
      <ContentWrapper>
        <Outlet />
      </ContentWrapper>
      <Footer />
    </>
  );
};
